package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.HomePage;
import java.time.Duration;

public class CardTovar {
    WebDriver driver;
    @FindBy(xpath = "//*[@class='t-store__prod-popup__btn t-btn t-btn_sm']")
    private WebElement korzina;
    public CardTovar(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public CardTovar waitTheFormToAppear2() {
        WebElement zadershka2 = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='t-store__prod-popup__btn t-btn t-btn_sm']")));
        return this;
    }
    public CardTovar addKorzina() {
        korzina.click();
        return this;
    }
}
