package page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
public class PageOformitZakaz {
    WebDriver driver;
    @FindBy (xpath = "//*[@id='input_1496239431201']")
    private WebElement nameInput;
    @FindBy (xpath = "(//*[@class='t-input t-input-phonemask'])[2]")
    private WebElement telephoneInput;
    @FindBy (xpath = "//*[@id='input_1627385047591']")
    private WebElement regionInput;
    @FindBy (xpath = "//*[@id='input_1630305196291']")
    private WebElement adressInput;
    @FindBy (xpath = "//*[@name='tildadelivery-city']")
    private WebElement cityInput;
    @FindBy (xpath = "//*[@class='t-radio__indicator']")
    private WebElement vyborDostavki;
    @FindBy (xpath = "//*[@name='tildadelivery-userinitials']")
    private WebElement recipient;
    @FindBy (xpath = "//*[@name='tildadelivery-street']")
    private WebElement street;
    @FindBy (xpath = "//*[@name='tildadelivery-house']")
    private WebElement house;
    @FindBy (xpath = "//*[@name='tildadelivery-aptoffice']")
    private WebElement office;
    @FindBy (xpath = "//*[@class='t-submit' and text()='ОФОРМИТЬ ЗАКАЗ']")
    private WebElement confirmBtn;
    public PageOformitZakaz(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public PageOformitZakaz waitTheFormToAppear3() {
        WebElement zadershka3 = new WebDriverWait(driver, Duration.ofSeconds(15))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@name='tildadelivery-userinitials']")));
        return this;
    }
    public PageOformitZakaz waitTheFormToAppear4() {
        WebElement zadershka4 = new WebDriverWait(driver, Duration.ofSeconds(15))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='t-submit' and text()='ОФОРМИТЬ ЗАКАЗ']")));
        return this;
    }
    public PageOformitZakaz enterUsername(String username) {
        nameInput.sendKeys(username);
        return this;
    }
    public PageOformitZakaz enterTelephone(String telephone) {
        telephoneInput.sendKeys(telephone);
        return this;
    }
    public PageOformitZakaz enterRegion(String region) {
        regionInput.sendKeys(region);
        return this;
    }
    public PageOformitZakaz enterAdress(String adress) {
        adressInput.sendKeys(adress);
        return this;
    }
    public PageOformitZakaz enterRecipient(String recipientName) {
        recipient.sendKeys(recipientName);
        return this;
    }
    public PageOformitZakaz enterStreet(String streetName) {
        street.sendKeys(streetName);
        return this;
    }
    public PageOformitZakaz enterHouse(String houseNumber) {
        house.sendKeys(houseNumber);
        return this;
    }
    public PageOformitZakaz enterOffice(String officeNumber) {
        office.sendKeys(officeNumber);
        return this;
    }

    public PageOformitZakaz clickConfirm() {
        confirmBtn.click();
        return this;
    }



}
