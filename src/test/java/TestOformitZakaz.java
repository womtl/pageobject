import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import page.AssertOformitZakaz;
import page.PageOformitZakaz;
import page.HomePage;
import page.CardTovar;

import javax.swing.*;

public class TestOformitZakaz {
    protected WebDriver driver;
    private PageOformitZakaz formPage;
    private AssertOformitZakaz assertPage;
    private HomePage home;
    private CardTovar card;



    @Before
    public void setup() {
        driver = new ChromeDriver();
        driver.manage().window().fullscreen();
        home = new HomePage(driver);
        card = new CardTovar(driver);
        formPage = new PageOformitZakaz(driver);
        assertPage = new AssertOformitZakaz(driver);
    }

    @After
    public void tearDown() {driver.quit();}
    @Test
    public void TestB() {
        SoftAssertions softAssert = new SoftAssertions();

        assertPage.openHomePage();
        home.waitTheFormToAppear1()
                .openFutbolka();
        card.waitTheFormToAppear2()
                .addKorzina();
        home.waitTheFormToAppear1()
                .voitiKorzina();
        home.waitTheFormToAppear1()
                .oformitZakaz();

        formPage.waitTheFormToAppear3()
                .enterUsername("Иванов Иван Иваныч")
                .enterRegion("Россия")
                .enterAdress("г. Москва, улица Саляма Адиля, д.77")
                .enterTelephone("(000) 000-00-00")
                .enterRecipient("Иванов Иван Иваныч")
                .enterStreet("ул Московская")
                .waitTheFormToAppear4()
                .enterOffice("12")
                .enterHouse("12")
                .clickConfirm();


        assertPage.errorBoxShouldBe(softAssert,"Укажите, пожалуйста, корректный номер телефона");



    }
}
