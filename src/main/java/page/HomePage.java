package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePage {
    WebDriver driver;
    @FindBy(xpath = "//*[@class='js-store-prod-name js-product-name t-store__card__title t-name t-name_md']")
    private WebElement futbolka;
    @FindBy (xpath = "//*[@class='t706__carticon-img']")
    private WebElement znachokKorzina;
    @FindBy(xpath = "//*[@class='t706__cartpage-open-form t-btn']")
    private WebElement knopkaOformitZakaz;
    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public HomePage waitTheFormToAppear1() {
        WebElement zadershka1 = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='js-store-prod-name js-product-name t-store__card__title t-name t-name_md']")));
        return this;
    }
    public HomePage openFutbolka() {
        futbolka.click();
        return this;
    }
    public HomePage voitiKorzina() {
        znachokKorzina.click();
        return this;
    }

    public HomePage oformitZakaz() {
        knopkaOformitZakaz.click();
        return this;
    }
}
